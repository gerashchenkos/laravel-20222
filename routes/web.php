<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AdminProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{id}', function ($id) {
    echo $id;
    return view('welcome');
    //return 'Hello World';
})->whereNumber('id');

Route::post('/', function () {
    //return view('welcome');
    return 'Hello World';
});

Route::get('/test/{id}', [TestController::class, 'index']);
Route::prefix('admin')->group(function () {
    Route::resource('products', AdminProductController::class);
});

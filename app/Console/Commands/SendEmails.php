<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test email';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->argument('email');
        if (empty($email)) {
            $email = $this->ask('What is recipient email?');
            $subject = $this->ask('Enter email subject?');
        }
        //$subject = $this->argument('subject') ?? "Hello";
        Mail::send([], [], function ($message) use ($email, $subject) {
            $message->to($email)
            ->subject($subject)
            // here comes what you want
            ->text('Hi, welcome user!'); // assuming text/plain
        });
        $this->info('Email was sent!');
        return 0;
    }
}
